#!/bin/bash

generate_post_data()
{
    cat <<EOF
{
 "vin":"$vehicleId",
 "session":"$sessionId",
 "timestamp":"$timestamp",
 "lat":"$lat",
 "heading":"$heading",
 "lon":"$lon"
}
EOF
}

SERVER_PROTOCOL="https://"
SERVER_ADDRESS="localhost"
SERVER_PORT=8443
username="testUser"
password="testPassword"
count=0
if [ -f "./data.csv" ]; then
    perl -p -i -e "s/\r//g" ./data.csv #remove this line if csv file line does not end with ^M
    while IFS=, read -r timestamp vehicleId sessionId lat lon heading
    do
        requestURL="$SERVER_PROTOCOL$SERVER_ADDRESS:$SERVER_PORT/vehicles/$vehicleId/sessions/$sessionId/locations"
        if [ $count -gt 0 ]; then
            tmp="1"
            timestamp=$tmp${timestamp:2:-4} #remove if timestamp is not in ###E+12 Mode
            timestamp=$((timestamp * 10 ** 7)) #remove if timestamp is not in ###E+12 Mode
            echo "$(generate_post_data)"
            (curl -u $username:$password -X POST $requestURL -H "Content-Type: application/json" -d "$(generate_post_data)" -k) &
            count=$((count + 1))
            sleep 0.1
        else
            count=$((count + 1))
        fi
        echo "Entering entry number "$count
    done < ./data.csv
else
    echo "File not found"
    exit 1
fi
exit 0