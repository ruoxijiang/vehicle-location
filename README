Vehicle Location Tracking

Description:

Project provides vehicle location tracking backend program.

Project assumptions:
1. Vehicle is identified by it's VIN, thus, VIN is unique.
2. Session id is a random generated string similar to UUID without the "-"s.
    Thus, the combination of these two are unique.
3. All "ordering/ordered" are in descending order.
4. The "correct ordering" of "all session of a vehicle" is the descending order which the session data is added to database.

Installation:

    System Requirement: Tested on Ubuntu 18.04 with openjdk-8 installed
    Database Requirement: Postgresql 10
    At project root directory, ./mvnw install to fetch project dependencies and build project

Getting started:

    Project config:
        In src/main/resource directory, edit "application.properties" file
            1 Edit "spring.datasource.url" field to match the created postgresql database, database server address.
            2 Edit "spring.datasource.username" and "spring.datasource.password" to match database password.
                Make sure the user have at least "all privilege" to configured database on first run.
                Once all tables are created, user privilege can be reduced to "SELECT, CREATE, UPDATE, DELETE" only.
            3 Change "server.port", "server.address" as needed.
            4 Default user name and password defined by "spring.security.user.name" and "spring.security.user.password"

    Running Project:
        In the project root directory, run ./mvnw spring-boot:run
        Optional, from the same directory, user can run ./populate.sh to populate database with data in the "data.csv"


Documentation:

Project uses httpBasic as authentication method for all end points.
All POST data and returned data are in json format, thus add "Content-Type: application/json" to all POST requests.
TLS certificate is self signed, thus need to make sure client does not validate certificate against root CA.

API:

    Add vehicle location data:
    Method: POST
    URL: /vehicles/{vin}/sessions/{sessionId}/locations
    PathParam:
        vin: String, vehicle identification number, alphanumerical only,
        sessionId: String, session string, alphanumerical only
    Body: All values are required
        {
            "vin": String, vehicle identification number, alphanumerical only, must match PathParam vin
            "session": String, session string, alphanumerical only, must match PathParam sessionId
            "timestamp": Long, unix timestamp
            "lat": String, a string representation of latitude,
            "lon": String, a string representation of longitude,
            "heading": Integer, an integer representing heading
         }
    Success Response Body:
        {
            "id": Long, entry id,
          "vin": String, vehicle identification number, alphanumerical only, must match PathParam vin
          "session": String, session string, alphanumerical only, must match PathParam sessionId
          "timestamp": Long, unix timestamp
          "lat": String, a string representation of latitude,
          "lon": String, a string representation of longitude,
          "heading": Integer, an integer representing heading
        }
    Error Response Body:
        {
            "timestamp": "2018-10-08T20:07:53.157+0000",
            "status": 409,
            "error": "Conflict",
            "message": "Vehicle ID not equal",
            "path": "/vehicles/TEIDFW2WBCXCGO6/sessions/eoue/locations"
        }
    Description:
        When sending location data for a vehicle for the first time, backend automatically adds the vehicle, vehicle session, and location data.
        If vehicle exists, backend only adds vehicle session, and location data.
        If vehicle, and vehicle session data exists, backend will only add location data.

    ---------------------------------------

    Get vehicle all session data:
        Method: GET
        URL: /vehicles/{vin}/sessions/{sessionId}/locations
        PathParam:
            vin: String, vehicle identification number, alphanumerical only,
            sessionId: String, session string, alphanumerical only
        Success Response Body:
            [
            {
                    "createdAt": "2018-10-08T20:15:17.195+0000", Datetime stamp, time added to backend
                    "id": 11,
                    "sessionId": "6bc6a660dfef4010ded079865f358e30", String, session id for vehicle location data
                },
            {
                    "createdAt": "2018-10-08T20:15:18.195+0000",
                    "id": 12,
                    "sessionId": "6bc6a660dfef4010ded579865f358e30"
                },
            ]
        Error Response Body:
            {
                "timestamp": "2018-10-08T20:07:53.157+0000",
                "status": 409,
                "error": "Conflict",
                "message": "Vehicle ID not equal",
                "path": "/vehicles/TEIDFW2WBCXCGO6/sessions/eoue/locations"
            }
        Description:
            If the "vin" exists, get vehicle identified by "vin", data ordered by session creation time, descending order.

        ---------------------------------------

    Get a single session of a vehicle of list of location data, ordered by timestamp:
       Method: GET
       URL: /vehicles/{vin}/sessions/{sessionId}/locations
       PathParam:
               vin: String, vehicle identification number, alphanumerical only,
               sessionId: String, session string, alphanumerical only
       Success Response Body:
            [
            {
              "id": Long, entry id,
              "vin": String, vehicle identification number, alphanumerical only
              "session": String, session string, alphanumerical only
              "timestamp": Long, unix timestamp
              "lat": String, a string representation of latitude,
              "lon": String, a string representation of longitude,
              "heading": Integer, an integer representing heading
            },
            {
              "id": Long, entry id,
              "vin": String, vehicle identification number, alphanumerical only
              "session": String, session string, alphanumerical only
              "timestamp": Long, unix timestamp
              "lat": String, a string representation of latitude,
              "lon": String, a string representation of longitude,
              "heading": Integer, an integer representing heading
            },
            ]
       Error Response Body:
            {
                "timestamp": "2018-10-08T20:07:53.157+0000",
                "status": 409,
                "error": "Conflict",
                "message": "Vehicle ID not equal",
                "path": "/vehicles/TEIDFW2WBCXCGO6/sessions/eoue/locations"
            }
       Description:
            Get vehicle location data, filter by vehicle vin and session id, order by timestamp descending order.

        ---------------------------------------

    Get last known vehicle location data:
       Method: GET
       URL: /vehicles/{vin}/lastKnownPosition
       PathParam:
               vin: String, vehicle identification number, alphanumerical only,
       Success Response Body:
            {
              "id": Long, entry id,
              "vin": String, vehicle identification number, alphanumerical only
              "session": String, session string, alphanumerical only
              "timestamp": Long, unix timestamp
              "lat": String, a string representation of latitude,
              "lon": String, a string representation of longitude,
              "heading": Integer, an integer representing heading
            }
       Error Response Body:
            {
                "timestamp": "2018-10-08T20:07:53.157+0000",
                "status": 404,
                "error": "Not Fount",
                "message": "Vehicle not found",
                "path": "/vehicles/TEIDFW2WBCXCGO6/sessions/eoue/locations"
            }
       Description:
            Get vehicle last known location data.
            If for whatever reason, there are two location data that have the same timestamp, only one will shown.
       ---------------------------------------