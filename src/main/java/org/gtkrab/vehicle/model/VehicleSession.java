package org.gtkrab.vehicle.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="vehicle_sessions",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"vehicle_id","session_id"})})
public class VehicleSession extends AuditModel{
    @Id
    @GeneratedValue
    private Long id;

    @Column(name="session_id")
    @NotBlank
    private String sessionId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name ="vehicle_id")
    @JsonIgnore
    private Vehicle vehicle;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }
}
