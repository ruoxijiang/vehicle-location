package org.gtkrab.vehicle.model;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="vehicle")
public class Vehicle extends AuditModel {
    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    @Column(unique = true)
    private String vin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
