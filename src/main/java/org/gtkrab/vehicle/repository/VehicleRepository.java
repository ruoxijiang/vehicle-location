package org.gtkrab.vehicle.repository;

import org.gtkrab.vehicle.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    Vehicle findVehicleByVin(String vin);
    boolean existsVehicleByVin(String vin);
}
