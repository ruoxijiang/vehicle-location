package org.gtkrab.vehicle.repository;

import org.gtkrab.vehicle.model.Location;
import org.gtkrab.vehicle.model.Vehicle;
import org.gtkrab.vehicle.model.VehicleSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
    List<Location> findAllByVehicleAndVehicleSessionOrderByTimestampDesc(Vehicle vehicle, VehicleSession vehicleSession);
    Location findFirstByVehicleOrderByTimestampDesc(Vehicle vehicle);
}
