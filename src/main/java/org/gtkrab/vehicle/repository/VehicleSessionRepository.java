package org.gtkrab.vehicle.repository;

import org.gtkrab.vehicle.model.Vehicle;
import org.gtkrab.vehicle.model.VehicleSession;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehicleSessionRepository extends JpaRepository<VehicleSession, Long> {
    VehicleSession findVehicleSessionBySessionId(String sessionId);
    boolean existsVehicleSessionBySessionId(String sessionId);
    VehicleSession findVehicleSessionByVehicleAndSessionId(Vehicle vehicle, String sessionId);
    boolean existsVehicleSessionByVehicleAndSessionId(Vehicle vehicle, String sessionId);
    List<VehicleSession> findVehicleSessionsByVehicleOrderByCreatedAtDesc(Vehicle vehicle);
}
