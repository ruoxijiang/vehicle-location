package org.gtkrab.vehicle.controller;

import org.gtkrab.vehicle.exception.ResourceConflictException;
import org.gtkrab.vehicle.model.Location;
import org.gtkrab.vehicle.model.VehicleSession;
import org.gtkrab.vehicle.model.transport.LocationDto;
import org.gtkrab.vehicle.service.LocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class LocationController {
    private static final Logger logger = LoggerFactory.getLogger(LocationController.class);
    @Autowired
    private LocationService locationService;

    @PostMapping("/vehicles/{vin:^[a-zA-Z0-9]+$}/sessions/{sessionId:^[a-zA-Z0-9]+$}/locations")
    public LocationDto createLocationData(@RequestBody LocationDto locationDto, @PathVariable("vin") String vin, @PathVariable("sessionId") String sessionId){
        if(!vin.equalsIgnoreCase(locationDto.getVin())){
            throw new ResourceConflictException("Vehicle ID not equal");
        }
        if(!sessionId.equalsIgnoreCase(locationDto.getSession())){
            throw new ResourceConflictException("Vehicle session ID not equal");
        }
        Location location;
//      Add nested try catch to solve issue of
//      same vehicle+session burst sending location data,
//      as simulated by the "populate.sh" script
        try {
            location = locationService.createLocationData(locationDto, true);
        }catch(Exception e){
            logger.info("Adding location data failed, retrying once");
            try {
                Thread.sleep(200);
                location = locationService.createLocationData(locationDto, false);
                logger.info("Adding location data succeeded after retry");
            }catch(Exception e2){
                try {
                    Thread.sleep(1000);
                    location = locationService.createLocationData(locationDto,false);
                    logger.info("Adding location data succeeded after retry");
                }catch(Exception e3){
                    logger.error("Adding location data failed third time", e3);
                    throw new ResourceConflictException("Location data conflicted, cannot add");
                }
            }

        }
        return LocationController.locationToLocationDto(location);
    }

    @GetMapping("/vehicles/{vin:^[a-zA-Z0-9]+$}/sessions/{sessionId:^[a-zA-Z0-9]+$}/locations")
    public List<LocationDto> getVehicleLocations(@PathVariable(name ="vin") String vin, @PathVariable("sessionId") String sessionId){
        List<Location> ret = locationService.getVehicleLocationsByVinAndSessionID(vin, sessionId);
        return LocationController.locationListToLocationDtoList(ret);
    }

    public static LocationDto locationToLocationDto(Location data){
        LocationDto dto = new LocationDto();
        if(data.getId() != null){
            dto.setId(data.getId());
        }
        dto.setHeading(data.getHeading());
        dto.setLat(data.getLat());
        dto.setLon(data.getLon());
        dto.setTimestamp(data.getTimestamp());
        if(data.getVehicle()!=null){
            VehicleSession vehicleSession = data.getVehicleSession();
            dto.setSession(vehicleSession.getSessionId());
            dto.setVin(vehicleSession.getVehicle().getVin());
        }
        return dto;
    }
    public static List<LocationDto> locationListToLocationDtoList(List<Location> data){
        List<LocationDto> dtos = new ArrayList<LocationDto>();
        for(Location location : data){
            dtos.add(LocationController.locationToLocationDto(location));
        }
        return dtos;
    }
}
