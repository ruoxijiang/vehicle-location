package org.gtkrab.vehicle.controller;

import org.gtkrab.vehicle.model.VehicleSession;
import org.gtkrab.vehicle.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class VehicleSessionController {
    @Autowired
    private VehicleService vehicleService;

    @GetMapping("/vehicles/{vin:^[a-zA-Z0-9]+$}/sessions")
    public List<VehicleSession> getVehicleSessionsOrderByCreateTime(@PathVariable(name ="vin") String vin){
        return vehicleService.getVehicleSessionsByVinOrderByCreateTime(vin);
    }
}
