package org.gtkrab.vehicle.controller;

import org.gtkrab.vehicle.model.Location;
import org.gtkrab.vehicle.model.transport.LocationDto;
import org.gtkrab.vehicle.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VehicleController {
    @Autowired
    private VehicleService vehicleService;

    @GetMapping("/vehicles/{vin:^[a-zA-Z0-9]+$}/lastKnownPosition")
    public LocationDto lastKnownLocationData(@PathVariable(name ="vin") String vin){
       Location location = vehicleService.getVehicleLastKnownLocationData(vin);
       return LocationController.locationToLocationDto(location);
    }
}
