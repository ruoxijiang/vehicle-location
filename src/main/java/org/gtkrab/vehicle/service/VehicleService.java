package org.gtkrab.vehicle.service;

import org.gtkrab.vehicle.exception.ResourceConflictException;
import org.gtkrab.vehicle.exception.ResourceNotFoundException;
import org.gtkrab.vehicle.model.Location;
import org.gtkrab.vehicle.model.Vehicle;
import org.gtkrab.vehicle.model.VehicleSession;
import org.gtkrab.vehicle.repository.LocationRepository;
import org.gtkrab.vehicle.repository.VehicleRepository;
import org.gtkrab.vehicle.repository.VehicleSessionRepository;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

@Service
public class VehicleService {

    @Autowired
    private VehicleRepository vehicleRepo;

    @Autowired
    private VehicleSessionRepository vehicleSessionRepo;

    @Autowired
    private LocationRepository locationRepository;

    @Transactional
    public VehicleSession createVehicleSession(String vin, String sessionId){
        Vehicle vehicle;
        VehicleSession vehicleSession;
        if(!vehicleRepo.existsVehicleByVin(vin.toLowerCase())){
            vehicle = new Vehicle();
            vehicle.setVin(vin.toLowerCase());
            try {
                vehicleRepo.save(vehicle);
            }catch(RuntimeException e){
                vehicle = vehicleRepo.findVehicleByVin(vin);
            }
        }else{
            vehicle = vehicleRepo.findVehicleByVin(vin);
        }
        if(!vehicleSessionRepo.existsVehicleSessionBySessionId(sessionId.toLowerCase())){
            vehicleSession = new VehicleSession();
            vehicleSession.setVehicle(vehicle);
            vehicleSession.setSessionId(sessionId.toLowerCase());
            vehicleSessionRepo.save(vehicleSession);
            return vehicleSession;
        }else{
            return vehicleSessionRepo.findVehicleSessionBySessionId(sessionId.toLowerCase());
        }
    }
    @Transactional(readOnly = true)
    public boolean exitsVehicleSessionByVinAndSessionId(String vin, String sessionId){
        if(!vehicleRepo.existsVehicleByVin(vin.toLowerCase())){
            return false;
        }
        Vehicle vehicle = vehicleRepo.findVehicleByVin(vin.toLowerCase());
        return vehicleSessionRepo.existsVehicleSessionByVehicleAndSessionId(vehicle, sessionId.toLowerCase());
    }
    @Transactional(readOnly = true)
    public boolean exitsVehicleByVin(String vin){
        return vehicleRepo.existsVehicleByVin(vin.toLowerCase());
    }

    @Transactional(readOnly = true)
    public Location getVehicleLastKnownLocationData(String vin){
        if(!this.exitsVehicleByVin(vin)){
            throw new ResourceNotFoundException("Requested vehicle dose not exist");
        }
        Vehicle vehicle = vehicleRepo.findVehicleByVin(vin.toLowerCase());
        return locationRepository.findFirstByVehicleOrderByTimestampDesc(vehicle);
    }

    @Transactional(readOnly = true)
    public List<VehicleSession> getVehicleSessionsByVinOrderByCreateTime(String vin){
        if(!vehicleRepo.existsVehicleByVin(vin.toLowerCase())){
            throw new ResourceNotFoundException("Vehicle not found");
        }
        Vehicle vehicle = vehicleRepo.findVehicleByVin(vin.toLowerCase());
        return vehicleSessionRepo.findVehicleSessionsByVehicleOrderByCreatedAtDesc(vehicle);

    }

    @Transactional(readOnly = true)
    public VehicleSession getVehicleSessionByVinAndSessionId(String vin, String sessionId){
        Vehicle vehicle;
        if(!vehicleRepo.existsVehicleByVin(vin.toLowerCase())){
            throw new ResourceNotFoundException("Vehicle not found");
        }
        vehicle = vehicleRepo.findVehicleByVin(vin.toLowerCase());
        if(!vehicleSessionRepo.existsVehicleSessionBySessionId(sessionId.toLowerCase())){
            throw new ResourceNotFoundException("Session for vehicle not found");
        }
        return vehicleSessionRepo.findVehicleSessionByVehicleAndSessionId(vehicle, sessionId.toLowerCase());
    }
}
