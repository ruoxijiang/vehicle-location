package org.gtkrab.vehicle.service;

import org.gtkrab.vehicle.exception.ResourceNotFoundException;
import org.gtkrab.vehicle.model.Location;
import org.gtkrab.vehicle.model.Vehicle;
import org.gtkrab.vehicle.model.VehicleSession;
import org.gtkrab.vehicle.model.transport.LocationDto;
import org.gtkrab.vehicle.repository.LocationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LocationService {
    private static final Logger logger = LoggerFactory.getLogger(LocationService.class);
    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private LocationRepository locationRepo;

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Location createLocationData(LocationDto locationDto, boolean newVehileSession){
        Location location;
        Vehicle vehicle;
        VehicleSession vehicleSession;
        if(newVehileSession) {
            if (!vehicleService.exitsVehicleSessionByVinAndSessionId(locationDto.getVin(), locationDto.getSession())) {
                vehicleSession = vehicleService.createVehicleSession(locationDto.getVin(), locationDto.getSession());
                vehicle = vehicleSession.getVehicle();
            } else {
                vehicleSession = vehicleService.getVehicleSessionByVinAndSessionId(locationDto.getVin(), locationDto.getSession());
                vehicle = vehicleSession.getVehicle();
            }
        }else{
            vehicleSession = vehicleService.getVehicleSessionByVinAndSessionId(locationDto.getVin(), locationDto.getSession());
            vehicle = vehicleSession.getVehicle();
        }
        location = LocationService.locationDtoToLocation(locationDto);
        location.setVehicle(vehicle);
        location.setVehicleSession(vehicleSession);
        locationRepo.save(location);
        return location;
    }

    @Transactional(readOnly = true)
    public List<Location> getVehicleLocationsByVinAndSessionID(String vin, String sessionId){
        if(vin==null || vin.isEmpty() || sessionId ==null || sessionId.isEmpty()){
            throw new ResourceNotFoundException("Requested vehicle with session not found");
        }
        if(!vehicleService.exitsVehicleSessionByVinAndSessionId(vin, sessionId)){
            throw new ResourceNotFoundException("Requested vehicle with session not found");
        }
        VehicleSession vehicleSession = vehicleService.getVehicleSessionByVinAndSessionId(vin, sessionId);
        return locationRepo.findAllByVehicleAndVehicleSessionOrderByTimestampDesc(vehicleSession.getVehicle(), vehicleSession);
    }

    public static Location locationDtoToLocation(LocationDto dto){
        Location ret = new Location();
        ret.setHeading(dto.getHeading());
        ret.setLat(dto.getLat());
        ret.setLon(dto.getLon());
        ret.setTimestamp(dto.getTimestamp());
        return ret;
    }
}
