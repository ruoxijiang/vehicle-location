package org.gtkrab.vehicle.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.CONFLICT)
public class ResourceConflictException extends RuntimeException {
    public ResourceConflictException(String s) {
        super(s);
    }

    public ResourceConflictException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
